import { Router } from "../routes";
import Alert from "react-s-alert";
import PropTypes from "prop-types";
import { Button, Jumbotron } from 'react-bootstrap';
import { i18n, Trans, withNamespaces } from "../i18n";

const now = new Date();
const Jumbo = ({ t, asPath }) => (
  <Jumbotron>
    <h1>This is for Movie Preview</h1>
    <p>
      This is a simple hero unit, a simple jumbotron-style component for calling
      extra attention to featured content or information.
    </p>
    <p>
      <Button variant="primary">Learn more</Button>
    </p>
  </Jumbotron>
);

Jumbo.propTypes = {
  t: PropTypes.func.isRequired,
};

export default withNamespaces("common")(Jumbo);

import { compose } from "redux"
import { connect } from 'react-redux'
import {
  first,
  get,
  isEmpty
} from 'lodash'
import { getCookie } from '../utils/cookies'
import Head from '../components/head'
import { withNamespaces } from '../i18n'
import numeral from '../utils/numeral'
import PropTypes from 'prop-types'
import React from 'react'
import noAuth from '../_hoc/noAuth'
import Jumbo from '../components/jumbo'
import {
  getMovieByCategory
} from '../stores/actions'


class Index extends React.Component {
  static async getInitialProps({store, req}) {
    const currentState = store.getState();
    const lang = getCookie('next-i18next', req) || 'en';

    await store.dispatch(getMovieByCategory('actions'));

    return {
      lang,
      namespacesRequired: ['common']
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      inBrowser: false
    }
  }

  componentDidMount() {
    this.setState({ inBrowser: true });
  }

render() {
  const {
    location,
    lang,
    t,
    movie
  } = this.props;

  console.log(movie)

  const {
    inBrowser
  } = this.state;
  return (
    <main>
      <Head title={`${t('current-weather')} | ${t('title')}`} />
      <Jumbo />
      <div className="row justify-content-center">
        <div className="col-md-auto col-lg-6">
          <h2 className="text-center m-b-24">{t('current-weather')}</h2>
        </div>
      </div>
      
      {!isEmpty(movie) && (
        <div>not empty</div>
      )}
    </main>
  )
}
}

Index.propTypes = {
  location: PropTypes.shape({
    lat: PropTypes.number,
    lng: PropTypes.number,
  }),
  lang: PropTypes.string,
  t: PropTypes.func,
  movie: PropTypes.shape({}),
}

const mapStateToProps = (state) => ({
  movie: state.movie.data
})

export default compose(
  connect(mapStateToProps),
  withNamespaces(['common']),
  noAuth(),
)(Index);

import { apiCall, apiUrl } from "../../services/request";
import { get } from 'lodash'
import getConfig from "next/config"
import {
  MOVIE_GET,
  MOVIE_GET_ERROR,
  MOVIE_GET_SUCCESS
} from "../actionTypes";

const { publicRuntimeConfig } = getConfig()
const { MOVIE_DB_API_KEY } = publicRuntimeConfig


export const getMovieByCategory = (category, page) => async dispatch => {
  const dataReq = {
    method: "GET",
    url: `https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=${MOVIE_DB_API_KEY}&page=${page}`,
  };
  dispatch({ type: MOVIE_GET });
  try {
    const res = await dispatch(apiCall(dataReq));
    if ( get(res, 'status') == 200 ) {
      dispatch({
        type: MOVIE_GET_SUCCESS,
        payload: {
          data: get(res, 'data')
        }
      });
      return res;
    }else{
      dispatch({ type: MOVIE_GET_ERROR });
    }
    return res;
  } catch (error) {
    dispatch({ type: MOVIE_GET_ERROR });
    return error;
  }
}

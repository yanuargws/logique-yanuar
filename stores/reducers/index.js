import { combineReducers } from "redux";


import alertReducer from "./alertReducer";
import movieReducer from "./movieReducer";

const reducers = combineReducers({
  alert: alertReducer,
  movie: movieReducer
});

export default reducers;